# Genie Garage Door Hack

This is a project to reverse engineer the Genie garage door openers. The purpose here is to then allow the Flipper Zero and other TX-able SDR's to be able to interface with these proprietary systems.

## What is this?

The basic RF protocol is in  [https://gitlab.com/crankylinuxuser/Genie-Garage-Door-hack/-/blob/main/genie_garage_door_protocol.txt]

The rolling code is KeeLoq according top patent data. However, according to anonymous posts, it appears to be a deviation away from standard keeloq to a custom build.

This work is intended for 3rd party compatibility with devices like the Flipper Zero and other SDR devices, for purposes of legitimate interoperation with Genie Garage door and barrier systems.

Images of the keyfob are in [https://gitlab.com/crankylinuxuser/Genie-Garage-Door-hack/-/tree/main/keyfob%20images] directory. 

The HackRF IQ data capture is a Universal Radio Hacker acquisition, using 2MHz/2MSPS capture centered at 389.5MHz . It's a big file, but essential in understanding the RF protocol.



## License
The license is TBA. Right now, I am in information gathering mode. 

This project will be also a firmware analysis, which I will be operating under Fair Use for purposes of interoperation.

